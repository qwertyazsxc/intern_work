# 겨울 계절제 현장실습 - 웹페이지 제작

--------------------------------------------------------------------------------

## 구성

- 컴퓨터학부 박정현, 홍창민

## 구동 전 준비

- mongoDB 설치: <https://www.mongodb.com/download-center?jmp=nav#community>
- `npm install`

## 구동 방법

1. mongoDB 구동: `db.cmd` 실행 (실행이 안 되면 환경 변수 확인)
2. 서버 구동: `start.cmd` 실행 또는 `npm start`
3. style 작업 시 (css파일이 없기 때문에 최초 1번은 실행 필요함): `watch-styles.cmd` 실행 (코드에 변화 있을 때마다 css 재생성)

## 출처

- 소셜 로그인: <https://codepen.io/eswarasai/pen/GZvJOE?q=social+login&limit=all&type=type-pens>

- 로그인 폼: [https://codepen.io/vikasverma93/pen/zplmq?depth=everythingℴ=popularity&page=2&q=login&show_forks=false](https://codepen.io/vikasverma93/pen/zplmq?depth=everything&order=popularity&page=2&q=login&show_forks=false)

- backgound transition: <https://github.com/mttrchrds/background-transition>

- 스크롤 다운 버튼: <https://www.nxworld.net/tips/css-scroll-down-button.html>, <https://codepen.io/anon/pen/bawrJN>

- 소셜 로그인: <https://www.npmjs.com/package/passport>

- 슬라이드 배너: <https://bxslider.com/>

- 파일 인풋 폼: <https://codepen.io/anon/pen/zpzVRd>

- 파일 업로드 미리 보기: <https://codepen.io/anon/pen/xpLxxq>

- 우편번호 검색: <http://postcode.map.daum.net/guide>

- 표정 아이콘: <https://www.flaticon.com/packs/emotions-rounded>

## 설정해야 할 사항

1. Google, Facebook의 개발자 사이트(<https://console.developers.google.com/>, <https://developers.facebook.com/)에> 접속해서 clientID, clientSecret를 받고 리디렉션 URI를 설정한 뒤 /src/config/passport.js의 해당 부분을 바꿔준다.

  ※ 참고: <http://blog.jeonghwan.net/passport-google-oauth-%EA%B5%AC%EA%B8%80-%EC%9D%B8%EC%A6%9D-%EB%AA%A8%EB%93%88/>

2. /admin에 접속해서 최초 관리자 계정을 생성한 뒤, 로그인해서 비밀번호를 변경한다. (최초 id 및 pw는 admin)
