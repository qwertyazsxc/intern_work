var clearValue = function() {
    $("input[type=text]").val('');
    $("input[type=password]").val('');
    $("input[type=date]").val('');
    $("input[type=email]").val('');
    $("input[type=radio]").prop('checked', false);
}

var validCheck = function(usertype, req) {
    switch (usertype) {
        case 'Personal':
            if (!/^[A-za-z][a-zA-Z0-9]{3,14}/g.test(req.username)) {
                alert('아이디를 다시 입력해 주세요.');
                return false;
            } else if (!/^[a-zA-Z0-9_]{4,20}$/.test(req.password)) {
                alert('비밀번호를 다시 입력해 주세요.');
                return false;
            } else if (req.password_re != req.password) {
                alert('비밀번호 재입력이 맞지 않습니다.');
                return false;
            } else if (req.birthdate == '') {
                alert('생년월일을 입력해 주세요.');
                return false;
            } else if (req.fullname == '') {
                alert('이름을 입력해 주세요.');
                return false;
            } else if (!/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/.test(req.phone)) {
                alert('휴대전화를 다시 입력해 주세요.');
                return false;
            } else if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(req.email)) {
                alert('이메일을 다시 입력해 주세요.');
                return false;
            } else if (req.gender == false) {
                alert('성별을 선택해 주세요.');
                return false;
            }
            break;

        case 'Company-manager':
            if (!/^[A-za-z][a-zA-Z0-9]{3,14}/g.test(req.username)) {
                alert('아이디를 다시 입력해 주세요.');
                return false;
            } else if (!/^[a-zA-Z0-9_]{4,20}$/.test(req.password)) {
                alert('비밀번호를 다시 입력해 주세요.');
                return false;
            } else if (req.password_re != req.password) {
                alert('비밀번호 재입력이 맞지 않습니다.');
                return false;
            } else if (req.companyName == '') {
                alert('단체 조회를 통해 단체명을 입력해 주세요.');
                return false;
            } else if (req.fullname == '') {
                alert('이름을 입력해 주세요.');
                return false;
            } else if (req.position == '') {
                alert('직책을 입력해 주세요.');
                return false;
            } else if (req.department == '') {
                alert('담당부서를 입력해 주세요.');
                return false;
            } else if (!/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/.test(req.phone)) {
                alert('휴대전화를 다시 입력해 주세요.');
                return false;
            } else if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(req.email)) {
                alert('이메일을 다시 입력해 주세요.');
                return false;
            }
            break;

        case 'Companyadd':
            if (req.companyName == '') {
                alert('단체명을 입력해 주세요.');
                return false;
            } else if (req.companyRep == '') {
                alert('대표자명을 입력해 주세요.');
                return false;
            } else if (req.businessReg == '') {
                alert('사업자 등록번호를 다시 입력해 주세요.');
                return false;
            } else if (!/^[0-6][0-9]{4}$/.test(req.companyPost)) {
                alert('우편번호 찾기를 통해 우편번호를 입력해 주세요.');
                return false;
            } else if (req.companyAddress == '') {
                alert('우편번호 찾기를 통해 단체주소를 입력해 주세요.');
                return false;
            } else if (req.companyAddressSpec == '') {
                alert('상세주소를 입력해 주세요.');
                return false;
            }
            break;
    }

    return true;
}

$(document).ready(function() {
    clearValue();
    $("#personal-top").addClass("active");
    $(".terms-div .terms-content").scrollTop(0);
    $(".backgroundTransition").backgroundTransition({
        backgrounds: [{
                src: "img/1.jpeg"
            },
            {
                src: "img/2.jpeg"
            },
            {
                src: "img/3.jpeg"
            },
            {
                src: "img/4.jpeg"
            },
            {
                src: "img/5.jpeg"
            },
            {
                src: "img/6.jpeg"
            },
            {
                src: "img/7.jpeg"
            }
        ],
        transitionDelay: 3,
        animationSpeed: 800
    });
});

$("#company-top").click(function() {
    $(".social-login").hide();
    clearValue();
    $("#personal-top").removeClass("active");
    $("#company-top").addClass("active");
});

$("#personal-top").click(function() {
    $(".social-login").show();
    clearValue();
    $("#company-top").removeClass("active");
    $("#personal-top").addClass("active");
});

$(".login-div-bottom").click(function() {
    $(".login-div").fadeOut(100);
    $(".top-text").html("약관 동의");
    $(".terms-div .terms-content").scrollTop(0);
    $(".terms-div").delay(100).fadeIn(100);
});

$(".terms-div-bottom").click(function() {
    $(".terms-div").fadeOut(100);
    $(".login-div").delay(100).fadeIn(100);
    $(".top-text").html("로그인");
    $(".terms-div .terms-content").scrollTop(0);
});

$(".personal-register-bottom").click(function() {
    $(".personal-register-form").fadeOut(100);
    // $(".backgroundTransition, .backgroundTransition .image-top, .backgroundTransition .image-bottom").css("height", "100%");
    $(".top-text").html("로그인");
    $(".login-div").delay(100).fadeIn(100);
});

$(".company-register-bottom").click(function() {
    $(".company-register-form").fadeOut(100);
    // $(".backgroundTransition, .backgroundTransition .image-top, .backgroundTransition .image-bottom").css("height", "100%");
    $(".top-text").html("로그인");
    $(".login-div").delay(100).fadeIn(100);
});

$(".complete-div-bottom").click(function() {
    $(".complete-div").fadeOut(100);
    $(".top-text").html("로그인");
    $(".login-div").delay(100).fadeIn(100);
});

$(".terms-agree-button").click(function(e) {
    $(".terms-div").fadeOut(100);
    if ($("#personal-top").hasClass("active")) {
        // $(".backgroundTransition, .backgroundTransition .image-top, .backgroundTransition .image-bottom").css("height", "115%");
        $(".top-text").html("개인 계정 등록");
        clearValue();
        $(".personal-register-form").delay(100).fadeIn(100);
    } else {
        $(".top-text").html("단체 계정 등록");
        clearValue();
        $(".company-register-form").delay(100).fadeIn(100);
    }
    e.preventDefault();
});

$('.button-popup').click(function() {
    $('.login-page').fadeOut(100);
    $('.company-popup').delay(100).fadeIn(100);
    $('.label-tablemessage').text('단체명을 입력하고 조회를 눌러 주세요.');
    $('.label-tablemessage').css('z-index', 1);
    $('.popup-content .search input[name=companyName]').val('');
    $('.companyTable tbody tr').remove();
});

$('.button-search').click(function() {
    searchCompany();
});

$('.popup-content .search input[name=companyName]').keydown(function(key) {
    if (key.keyCode == 13) {
        searchCompany();
    }
});

var searchCompany = function() {
    var companyName = $('.popup-content .search input[name=companyName]').val();

    if (companyName == '') {
        alert('단체명을 입력해 주세요.');
        return;
    }

    $.post('/companySearch', {
        'companyName': companyName
    }, function(data, status) {
        $('.companyTable tbody tr').remove();

        if (data.company.length <= 0) {
            $('.label-tablemessage').text('조회하신 단체를 찾을 수 없습니다.');
            $('.label-tablemessage').css('z-index', 1);
        } else {
            $('.label-tablemessage').text('단체명을 입력하고 조회를 눌러 주세요.');
            $('.label-tablemessage').css('z-index', -1);
        }

        for (i = 0; i < data.company.length; i++) {
            $('.companyTable tbody').append('<tr class="row"><td>' + (i + 1).toString() + '</td><td>' + data.company[i].companyName + '</td><td>' + data.company[i].companyRep + '</td></tr>');
        }
    });
};

$(".companyTable tbody").delegate('tr', 'click', function() {
    $('.company-register-form input[name=companyName]').val($(this).find('td:nth-child(2)').text());
    $('.company-popup').fadeOut(100);
    $('.login-page').delay(100).fadeIn(100);
});

$('.popup-content .button-cancel').click(function() {
    $('.company-popup').fadeOut(100);
    $('.login-page').delay(100).fadeIn(100);
});

$('.add-company-a').click(function() {
    $('.popup-content').fadeOut(100);
    $('.company-popup').css('height', '500px');
    $('.add-company').delay(100).fadeIn(100);
});

$('.company-add-form .button-submit').click(function(e) {
    e.preventDefault();

    var req = {
        'companyName': $('.company-add-form input[name=companyName]').val(),
        'companyRep': $('.company-add-form input[name=companyRep]').val(),
        'businessReg': $('.company-add-form input[name=businessReg]').val(),
        'companyPost': $('.company-add-form input[name=companyPost]').val(),
        'companyAddress': $('.company-add-form input[name=companyAddress]').val(),
        'companyAddressSpec': $('.company-add-form input[name=companyAddressSpec]').val()
    };

    if (!validCheck('Companyadd', req)) return;

    $.post('/companyAdd', {
        'companyName': req.companyName,
        'companyRep': req.companyRep,
        'businessReg': req.businessReg,
        'companyPost': req.companyPost,
        'companyAddress': req.companyAddress,
        'companyAddressSpec': req.companyAddressSpec
    }).done(function() {
        alert('단체 등록에 성공했습니다.');
        $('.add-company input').val('');
    }).fail(function() {
        alert('단체 등록에 실패했습니다.');
    }).always(function() {
        $('.add-company').fadeOut(100);
        $('.company-popup').css('height', '600px');
        $('.popup-content').delay(100).fadeIn(100);
    });
});

$('.add-company .button-cancel').click(function() {
    $('.add-company').fadeOut(100);
    $('.company-popup').css('height', '600px');
    $('.popup-content').delay(100).fadeIn(100);

    $('.add-company input').val('');
});

$(".personal-register-button").click(function(e) {
    $(".personal-register-div").fadeOut(100);
    // $(".backgroundTransition, .backgroundTransition .image-top, .backgroundTransition .image-bottom").css("height", "100%");
    $(".top-text").html("완료");
    $(".complete-div").delay(100).fadeIn(100);
    e.preventDefault();
});

$(".company-register-button").click(function(e) {
    $(".company-register-div").fadeOut(100);
    $(".top-text").html("완료");
    $(".complete-div").delay(100).fadeIn(100);
    e.preventDefault();
});

$('.personal-register-form').submit(function(e) {
    e.preventDefault();

    var req = {
        'username': $('.personal-register-form input[name=username]').val(),
        'password': $('.personal-register-form input[name=password]').val(),
        'password_re': $('.personal-register-form input[name=password-re]').val(),
        'birthdate': $('.personal-register-form input[name=birthdate]').val(),
        'fullname': $('.personal-register-form input[name=fullname]').val(),
        'phone': $('.personal-register-form input[name=phone]').val(),
        'email': $('.personal-register-form input[name=email]').val(),
        'gender': $('.personal-register-form input[name=gender]').is(':checked'),
        'auth': $('.personal-register-form input[name=auth]').val()
    };

    if (!validCheck('Personal', req)) return;

    this.submit();
});

$('.company-register-form').submit(function(e) {
    e.preventDefault();

    var req = {
        'username': $('.company-register-form input[name=username]').val(),
        'password': $('.company-register-form input[name=password]').val(),
        'password_re': $('.company-register-form input[name=password-re]').val(),
        'companyName': $('.company-register-form input[name=companyName]').val(),
        'fullname': $('.company-register-form input[name=fullname]').val(),
        'position': $('.company-register-form input[name=position]').val(),
        'department': $('.company-register-form input[name=department]').val(),
        'phone': $('.company-register-form input[name=phone]').val(),
        'email': $('.company-register-form input[name=email]').val(),
        'auth': $('.company-register-form input[name=auth]').val()
    };

    if (!validCheck('Company-manager', req)) return;

    this.submit();
});

$('.post-search').click(function(e) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            fullAddr = data.roadAddress;

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            //법정동명이 있을 경우 추가한다.
            if (data.bname !== '') {
                extraAddr += data.bname;
            }
            // 건물명이 있을 경우 추가한다.
            if (data.buildingName !== '') {
                extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
            fullAddr += (extraAddr !== '' ? ' (' + extraAddr + ')' : '');

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            $('.companyPost').val(data.zonecode); //5자리 새우편번호 사용
            $('.companyAddress').val(fullAddr);

            // 커서를 상세주소 필드로 이동한다.
            $('.companyAddressSpec').focus();
        }
    }).open();
});

$('.login-button').click(function(e) {
    e.preventDefault();

    var usertype = $('#personal-top').hasClass('active') ? 'Personal' : 'Company';

    post('/login', {
        'usertype': usertype,
        'username': $('.logintry-form input[name=username]').val(),
        'password': $('.logintry-form input[name=password]').val()
    });
});
