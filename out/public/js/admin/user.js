function findQuery(type, query) {
    $.post('/admin/user', {
        'type': type,
        'query': query
    }, function(data, status) {
        $('.basic-table tbody tr').remove();

        if (!data.user || data.user.length <= 0) {
            alert('해당하는 결과가 없습니다.');
            return;
        }

        resultArr = [];

        for (i = 0; i < data.user.length; i++) {
            var queryResult = {
                usertype: (data.user[i].usertype == 'Personal' ? '개인' : (data.user[i].usertype == 'Facebook' ? '개인(페이스북)' : (data.user[i].usertype == 'Google' ? '개인(구글)' : (data.user[i].usertype == 'Company-manager' ? '단체(담당자)' : (data.user[i].usertype == 'Company' ? '단체(구성원)' : 'unknown'))))),
                username: data.user[i].username,
                fullname: data.user[i].fullname,
                phone: data.user[i].phone,
                email: data.user[i].email,
                gender: (data.user[i].gender == 'male' ? '남자' : (data.user[i].gender == 'female' ? '여자' : 'unknown')),
                chargeAmt: data.user[i].chargeAmt
            };

            resultArr.push(queryResult);
        }

        for (i = 0; i < resultArr.length; i++) {
            $('.basic-table tbody').append('<tr class="row"><td>' + (i + 1).toString() + '</td><td>' + resultArr[i].usertype + '</td><td>' + resultArr[i].username + '</td><td>' + resultArr[i].fullname + '</td><td>' + resultArr[i].phone + '</td><td>' + resultArr[i].email + '</td><td>' + resultArr[i].gender + '</td></tr>');
        }
    });
}

$('.type-select').change(function() {
    $('.query-input').val('');
});

$('.search-button').click(function() {
    if ($('.type-select').val() == 'default') {
        findQuery('default', '');
    } else {
        findQuery($('.type-select').val(), $('.query-input').val());
    }
});

$('input').keydown(function(e) {
    if (e.keyCode === 13) {
        if ($('.type-select').val() == 'default') {
            findQuery('default', '');
        } else {
            findQuery($('.type-select').val(), $('.query-input').val());
        }
    }
});

$('label').click(function(e) {
    var order;

    if (temp == e.target.className) {
        order = -1;
        temp = 'default';
    } else {
        order = 1;
        temp = e.target.className;
    }
    resultArr.sort(function(a, b) {
        return order * (a[e.target.className] < b[e.target.className] ? -1 : 1);
    });

    $('.basic-table tbody tr').remove();

    for (i = 0; i < resultArr.length; i++) {
        $('.basic-table tbody').append('<tr class="row"><td>' + (i + 1).toString() + '</td><td>' + resultArr[i].usertype + '</td><td>' + resultArr[i].username + '</td><td>' + resultArr[i].fullname + '</td><td>' + resultArr[i].phone + '</td><td>' + resultArr[i].email + '</td><td>' + resultArr[i].gender + '</td></tr>');
    }
});

$('.basic-table tbody').click(function(e) {
    var currentRow = resultArr[e.target.parentElement.rowIndex - 1];

    $('.popup-layer td.usertype').html(currentRow.usertype);
    $('.popup-layer td.username').html(currentRow.username);
    $('.popup-layer td.fullname').html(currentRow.fullname);
    $('.popup-layer td.phone').html(currentRow.phone);
    $('.popup-layer td.email').html(currentRow.email);
    $('.popup-layer td.gender').html(currentRow.gender);
    $('.popup-layer td.chargeAmt').html('-');
    $('.popup-layer td.chargeAmt').html(currentRow.chargeAmt);
    $('.popup-layer').show();
});

$('.popup-layer .close').click(function() {
    $('.popup-layer').hide();
});

var temp = 'default';
var resultArr = [];
findQuery('default', '');
