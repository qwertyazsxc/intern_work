$('.logout-button').click(function(e) {
    location.href = '/signout';

    e.preventDefault();
});

$(".fold-button").click(function() {
    $(".sideMenu").toggleClass('active');
    $(".content").toggleClass('active');
    $(".fold-button").toggleClass('active');
    $(".popup-layer").toggleClass('active');

    if ($('.sideMenu').hasClass('active')) {
        $('.fold-button i').removeClass('fa fa-chevron-left');
        $('.fold-button i').addClass('fa fa-chevron-right');
    } else {
        $('.fold-button i').removeClass('fa fa-chevron-right');
        $('.fold-button i').addClass('fa fa-chevron-left');
    }
});

$(".mark").click(function() {
    location.href = '/admin';
});
$(".user-a").click(function() {
    location.href = "/admin/user"
});
$(".company-a").click(function() {
    location.href = "/admin/company"
});
$(".email-a").click(function() {
    location.href = "/admin/email"
});
$(".password-a").click(function() {
    location.href = "/admin/password"
});
