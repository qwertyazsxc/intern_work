function findQuery(type, query) {
    $.post('/admin/company', {
        'type': type,
        'query': query
    }, function(data, status) {
        $('.basic-table tbody tr').remove();

        if (!data.company || data.company.length <= 0) {
            alert('해당하는 결과가 없습니다.');
            return;
        }

        resultArr = [];

        for (i = 0; i < data.company.length; i++) {
            var queryResult = {
                companyName: data.company[i].companyName,
                companyRep: data.company[i].companyRep,
                businessReg: data.company[i].businessReg,
                companyPost: data.company[i].companyPost,
                companyAddress: data.company[i].companyAddress,
                companyAddressSpec: data.company[i].companyAddressSpec,
                groups: data.company[i].groups
            };

            resultArr.push(queryResult);
        }

        for (i = 0; i < resultArr.length; i++) {
            $('.basic-table tbody').append('<tr class="row"><td>' + (i + 1).toString() + '</td><td>' + resultArr[i].companyName + '</td><td>' + resultArr[i].companyRep + '</td><td>' + resultArr[i].businessReg + '</td><td>' + resultArr[i].companyAddress + '</td><td>' + resultArr[i].companyAddressSpec + '</td></tr>');
        }
    });
}

$('.type-select').change(function() {
    $('.query-input').val('');
});

$('.search-button').click(function() {
    if ($('.type-select').val() == 'default') {
        findQuery('default', '');
    } else {
        findQuery($('.type-select').val(), $('.query-input').val());
    }
});

$('input').keydown(function(e) {
    if (e.keyCode === 13) {
        if ($('.type-select').val() == 'default') {
            findQuery('default', '');
        } else {
            findQuery($('.type-select').val(), $('.query-input').val());
        }
    }
});

$('label').click(function(e) {
    var order;

    if (temp == e.target.className) {
        order = -1;
        temp = 'default';
    } else {
        order = 1;
        temp = e.target.className;
    }
    resultArr.sort(function(a, b) {
        return order * (a[e.target.className] < b[e.target.className] ? -1 : 1);
    });

    $('.basic-table tbody tr').remove();

    for (i = 0; i < resultArr.length; i++) {
        $('.basic-table tbody').append('<tr class="row"><td>' + (i + 1).toString() + '</td><td>' + resultArr[i].companyName + '</td><td>' + resultArr[i].companyRep + '</td><td>' + resultArr[i].businessReg + '</td><td>' + resultArr[i].companyAddress + '</td><td>' + resultArr[i].companyAddressSpec + '</td></tr>');
    }
});

$('.basic-table tbody').click(function(e) {
    var currentRow = resultArr[e.target.parentElement.rowIndex - 1];

    $('.popup-layer td.companyName').html(currentRow.companyName);
    $('.popup-layer td.companyRep').html(currentRow.companyRep);
    $('.popup-layer td.businessReg').html(currentRow.businessReg);
    $('.popup-layer td.companyPost').html(currentRow.companyPost);
    $('.popup-layer td.companyAddress').html(currentRow.companyAddress);
    $('.popup-layer td.companyAddressSpec').html(currentRow.companyAddressSpec);

    $('.popup-layer').show();
});

$('.popup-layer .close').click(function() {
    $('.popup-layer').hide();
});

var temp = 'default';
var resultArr = [];
findQuery('default', '');
