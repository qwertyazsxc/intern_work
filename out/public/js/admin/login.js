$('.login-button').click(function() {
    post('/admin/login', {
        usertype: 'Admin',
        username: $('input[name=username]').val(),
        password: $('input[name=password]').val()
    });
});

$('input').keydown(function(e) {
    if (e.keyCode === 13) {
        post('/admin/login', {
            usertype: 'Admin',
            username: $('input[name=username]').val(),
            password: $('input[name=password]').val()
        });
    }
});
