function handleImgFileSelect(e) {
    // 이미지 정보들을 초기화
    var sel_files = [];
    $(".imgs_wrap").empty();

    var files = e.target.files;
    var filesArr = Array.prototype.slice.call(files);

    var index = 0;
    filesArr.forEach(function(f) {
        if (!f.type.match("image.*")) {
            alert("확장자는 이미지 확장자만 가능합니다.");
            return;
        }

        sel_files.push(f);

        var reader = new FileReader();
        reader.onload = function(e) {
            var html = "<a href=\"javascript:void(0);\" onclick=\"deleteImageAction(" + index + ")\" id=\"img_id_" + index + "\"><img src=\"" + e.target.result + "\" data-file='" + f.name + "' class='selProductFile' title='Click to remove'></a>";
            $(".file_wrap").append(html);
            index++;
        }
        reader.readAsDataURL(f);
        $(".file_wrap").css({
            "height": "auto",
        });
    });
}

function deleteImageAction(index) {
    console.log("index : " + index);
    console.log("sel length : " + sel_files.length);

    sel_files.splice(index, 1);

    var img_id = "#img_id_" + index;
    $(img_id).remove();
}

function findQuery(type, query) {
    $.post('/main/members', {
        'type': type,
        'query': query
    }, function(data, status) {
        $('#left-table tbody tr').remove();

        if (!data.members || data.members.length <= 0) {
            alert('해당하는 결과가 없습니다.');
            return;
        }

        leftArr = [];

        for (i = 0; i < data.members.length; i++) {
            var queryResult = {
                username: data.members[i].username,
                fullname: data.members[i].fullname,
                birthdate: data.members[i].birthdate
            };

            leftArr.push(queryResult);
        }

        for (i = 0; i < leftArr.length; i++) {
            $('#left-table tbody').append('<tr><td><label class="chk-container"><input type="checkbox" name="chn"><span class="checkmark"></span></label></td><td>' + leftArr[i].fullname + '</td><td>' + toFormattedBirthDate(leftArr[i].birthdate) + '</td></tr>');
        }
    });
}

$(document).ready(function() {
    $("#input_imgs").on("change", handleImgFileSelect);
});

$('.type-select').change(function() {
    $('.query-input').val('');
});

$('.type-select').change(function() {
    $('.query-input').val('');
});

$('.search-container .search-button').click(function() {
    if ($('.type-select').val() == 'default') {
        findQuery('default', '');
    } else {
        findQuery($('.type-select').val(), $('.query-input').val());
    }
});

$('.search-container input').keydown(function(e) {
    if (e.keyCode === 13) {
        if ($('.type-select').val() == 'default') {
            findQuery('default', '');
        } else {
            findQuery($('.type-select').val(), $('.query-input').val());
        }
    }
});

$("#left-table .all-check").click(function(e) {
    if (!leftflag) {
        $("#left-table input[name=chn]").prop("checked", true);
        leftflag = true;
    } else if (leftflag) {
        $("#left-table input[name=chn]").prop("checked", false);
        leftflag = false;
    }
});

$("#right-table .all-check").click(function(e) {
    if (!rightflag) {
        $("#right-table input[name=chn]").prop("checked", true);
        rightflag = true;
    } else if (rightflag) {
        $("#right-table input[name=chn]").prop("checked", false);
        rightflag = false;
    }
});

$('.append-button').click(function() {
    $('#left-table tbody input:checkbox[name=chn]').each(function(e) {
        let selectedRow = $(this).parent().parent().parent()[0].rowIndex - 1;
        if (this.checked == true && $.inArray(leftArr[selectedRow], rightArr) === -1) {
            rightArr.push(leftArr[selectedRow]);
            this.checked = false;
        }
    });
    leftflag = false;

    $('#right-table tbody tr').remove();

    for (i = 0; i < rightArr.length; i++) {
        $('#right-table tbody').append('<tr><td><label class="chk-container"><input type="checkbox" name="chn"><span class="checkmark"></span></label></td><td>' + rightArr[i].fullname + '</td><td>' + toFormattedBirthDate(rightArr[i].birthdate) + '</td><td><p>파일 업로드</p></td></tr>');
    }

    $('.chkupNum').html(rightArr.length);
    $('.chkupAmt').html(toFormattedMoney(rightArr.length * 10000));
});

$('.delete-button').click(function() {
    let deleteOffset = 0;
    $('#right-table tbody input:checkbox[name=chn]').each(function(e) {
        let selectedRow = $(this).parent().parent().parent()[0].rowIndex - 1;
        if (this.checked == true) {
            rightArr.splice(selectedRow - deleteOffset++, 1);
            this.checked = false;
        }
    });
    rightflag = false;

    $('#right-table tbody tr').remove();

    for (i = 0; i < rightArr.length; i++) {
        $('#right-table tbody').append('<tr><td><label class="chk-container"><input type="checkbox" name="chn"><span class="checkmark"></span></label></td><td>' + rightArr[i].fullname + '</td><td>' + toFormattedBirthDate(rightArr[i].birthdate) + '</td><td><p>파일 업로드</p></td></tr>');
    }

    $('.chkupNum').html(rightArr.length);
    $('.chkupAmt').html(toFormattedMoney(rightArr.length * 10000));
});
$('.file-select-button').click(function(e) {
    $("#input_imgs").trigger('click');

    e.preventDefault();
});

var leftArr = [];
var rightArr = [];
var leftflag = false;
var rightflag = false;
findQuery('default', '');
