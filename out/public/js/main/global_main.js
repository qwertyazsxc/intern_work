var toFormattedMoney = function(money) {
    return money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

var toFormattedBirthDate = function(date) {
    let birthdate = new Date(date);

    return birthdate.getFullYear().toString() + '-' + (birthdate.getMonth() + 1).toString() + '-' + birthdate.getDate().toString();
};

$('.logout-button').click(function(e) {
    location.href = '/signout';

    e.preventDefault();
});

$(".fold-button").click(function() {
    $(".sideMenu").toggleClass('active');
    $(".content").toggleClass('active');
    $(".fold-button").toggleClass('active');
    $(".site-footer").toggleClass('active');

    if ($('.sideMenu').hasClass('active')) {
        $('.fold-button i').removeClass('fa fa-chevron-left');
        $('.fold-button i').addClass('fa fa-chevron-right');
    } else {
        $('.fold-button i').removeClass('fa fa-chevron-right');
        $('.fold-button i').addClass('fa fa-chevron-left');
    }
});

//메인화면
$(".mark").click(function() {
    location.href = '/main';
});

//내 정보
$(".info-a").click(function() {
    $(".info-a .subMenu").animate({
        height: "toggle",
        opacity: "toggle"
    }, "fast");
});

//결제 관리
$(".mangement-a").click(function() {
    $(".mangement-a .subMenu").animate({
        height: "toggle",
        opacity: "toggle"
    }, "fast");;
});

//구성원 관리
$(".member-a").click(function() {
    $(".member-a .subMenu").animate({
        height: "toggle",
        opacity: "toggle"
    }, "fast");;
});

//선불 충전
$(".payment-pre").click(function() {
    location.href = '/main/prepayment';
    event.stopPropagation();
});

//결제내역
$(".payhistory-a").click(function() {
    location.href = '/main/payhistory';
    event.stopPropagation();
});

//구성원 등록
$(".member-register").click(function() {
    location.href = '/main/memreg';
    event.stopPropagation();
});

//구성원 조회
$(".member-lookup").click(function() {
    location.href = '/main/memlookup';
    event.stopPropagation();
});

//검사신청
$(".checkup-a").click(function() {
    location.href = '/main/checkup';
    event.stopPropagation();
});

//결과확인
$(".result-a").click(function() {
    location.href = '/main/result';
});
