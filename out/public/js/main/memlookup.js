var flag = 0;

$("#all-check").click(function(e) {
    if (flag == 0) {
        $("#lookup-table input[name=chn]").prop("checked", true);
        $('tr').css("background", "grey");
        flag = 1;
    } else if (flag == 1) {
        $("#lookup-table input[name=chn]").prop("checked", false);
        $(this).parent().parent().parent().css("background", "white");
        $('tr').css("background", "white");
        flag = 0;
    }
});

$("input:checkbox[name=chn]").on('click', function() {
    if ($(this).prop('checked')) {
        $(this).parent().parent().parent().css("background", "grey");
    } else {
        $(this).parent().parent().parent().css("background", "white");
    }
});
