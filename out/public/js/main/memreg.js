var flag = 0;

var validCheck = function(req) {
    if (req.fullname == '') {
        alert('이름을 입력해 주세요.');
        return false;
    } else if (!/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/.test(req.phone)) {
        alert('휴대전화를 다시 입력해 주세요.');
        return false;
    } else if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(req.email)) {
        alert('이메일을 다시 입력해 주세요.');
        return false;
    } else if (req.gender != 'male' && req.gender != 'female') {
        alert('성별을 선택해 주세요.');
        return false;
    }

    return true;
}

$("#all-check").click(function(e) {

    if ($('input:checkbox[name=chn]').length == 0) {
        alert("체크 할 항목이 존재하지 않습니다.");
    } else {
        if (flag == 0) {
            $(".member-table input[name=chn]").prop("checked", true);
            flag = 1;
        } else if (flag == 1) {
            $(".member-table input[name=chn]").prop("checked", false);
            flag = 0;
        }
    }
});

$("#delete-button").click(function() {
    if ($('input:checkbox[name=chn]').length == 0) {
        alert("삭제 할 항목이 존재하지 않습니다.");
    } else {
        if ($("input:checkbox[name=chn]:checked").length == 0) {
            alert('삭제할 항목을 눌러주세요');
        } else {
            $('.member-table tbody input:checkbox[name=chn]').each(function(e) {
                if (this.checked == true) {
                    var row = $(this).parent().parent().parent().index();
                    $(this).parent().parent().parent().remove();
                    Arr.splice(row, 1);
                }
            });
        }
    }
    $("#all-check").prop("checked", false);
});



$('.member-table').on('click', 'p', function() {
    $('.overlay-t, .layer-popup').show();
    $('.layer-popup').css("top", Math.max(0, $(window).scrollTop() + 100) + "px");

    $('.overlay_t, .close').click(function(e) {
        e.preventDefault();
        $('.overlay-t, .layer-popup').hide();
    });

    var row = $(this).parent().parent().index();

    $('#td-fullname').text(Arr[row].fullname);
    $('#td-birth').text(Arr[row].birthdate);
    $('#td-phone').text(Arr[row].phone);
    $('#td-email').text(Arr[row].email);
    if (Arr[row].gender == 'male') {
        $('#td-gender').text('남자');
    } else {
        $('#td-gender').text('여자');
    }
});
