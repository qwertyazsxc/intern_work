$('#temper-toggle').click(function() {
    if ($('#temper-toggle').hasClass('fa fa-toggle-down')) {
        $('.temper-list').css('height', '120px');
        $('.temper-list').css('padding', '20px');
        $('.temper-list').css('visibility', 'visible');

        $('#temper-toggle').removeClass('fa fa-toggle-down');
        $('#temper-toggle').addClass('fa fa-toggle-up');
    } else {
        $('.temper-list').css('height', '0');
        $('.temper-list').css('padding', '0 20px');
        $('.temper-list').css('visibility', 'hidden');

        $('#temper-toggle').removeClass('fa fa-toggle-up');
        $('#temper-toggle').addClass('fa fa-toggle-down');
    }
});
