//엔터입력시 submit 방지
$('input[type="number"]').keydown(function() {
    if (event.keyCode === 13) {
        event.preventDefault();
    }
});

//숫자만 들어가는 input 함수.
function onlyNumber(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    var number = $(".amount-money").val();
    $("#output").html(toFormattedMoney(number) + "원");
    if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
        return;
    else
        return false;
}

function removeChar(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    var number = $(".amount-money").val();
    $("#output").html(toFormattedMoney(number) + "원");
    if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
        return;
    else
        event.target.value = event.target.value.replace(/[^0-9]/g, "");
}
