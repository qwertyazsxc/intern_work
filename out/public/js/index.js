$(document).ready(function() {
    $('html').scrollTop(0);
    $('.backgroundTransition').backgroundTransition({
        backgrounds: [{
                src: "img/1.jpeg"
            },
            {
                src: "img/2.jpeg"
            },
            {
                src: "img/3.jpeg"
            },
            {
                src: "img/4.jpeg"
            },
            {
                src: "img/5.jpeg"
            },
            {
                src: "img/6.jpeg"
            },
            {
                src: "img/7.jpeg"
            }
        ],
        transitionDelay: 3,
        animationSpeed: 800
    });
});

//스크롤
$(function() {
    $('.scroll').on('click', function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'swing'); //속도
    });
});


//구글 맵
function initMap() {
    var knu_mobile_tech = {
        lat: 35.8920288,
        lng: 128.6142224
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: knu_mobile_tech
    });
    var marker = new google.maps.Marker({
        position: knu_mobile_tech,
        map: map
    });
}
