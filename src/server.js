var express = require('express');
var port = 8080;
var path = require('path');
var nodemailer = require('nodemailer');
var dbConfig = require('./config/db');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var moment = require('moment');

// add timestamps in front of log messages
require('console-stamp')(console, "yyyy-mm-dd HH:MM:ss 'UTC'o");

mongoose.connect(dbConfig.url);
//데이터베이스 접속 확인
mongoose.connection.on('error', console.error.bind(console, 'Connection error:'));
mongoose.connection.once('open', function(callback) {
    console.log("mongoDB connected")
});

var app = express();

app.set('views', path.join(__dirname, '/../out/views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(path.join(__dirname, '/../out/public')));

var passport = require('passport');
app.use(cookieParser());
var session = require('express-session');
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); //로그인 세션 유지

//플래시메세지를 사용한다면
var flash = require('connect-flash');
app.use(flash());

var passportConfig = require('./config/passport');
passportConfig(passport);

var pdf = require('express-pdf');
app.use(pdf);

var route = require('./route/index')(passport);
app.use('/', route);
var route_main = require('./route/main')(passport);
app.use('/main', route_main);
var route_admin = require('./route/admin')(passport);
app.use('/admin', route_admin);

app.locals.toFormattedMoney = function(money) {
    return money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
};
app.locals.toFormattedDate = function(date) {
    return moment(date).format('YYYY-MM-DD[\n]A hh:mm:ss');
};
app.locals.toFormattedBirthDate = function(date) {
    return moment(date).format('YYYY-MM-DD[\n]');
};
app.locals.toFormattedGender = function(gender) {
    if (gender == 'male') {
        return gender.replace('male', '남자');
    } else if (gender == 'female') {
        return gender.replace('female', '여자');
    }
};

app.use((req, res, next) => { // 404 처리 부분
    res.status(404).redirect('/notfound');
});

var server = app.listen(port, function() {
    console.log('server listening on port ' + port);
});

module.exports = app;
