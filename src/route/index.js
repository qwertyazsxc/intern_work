var express = require('express');
var router = express.Router();
var path = require('path');
var Company = require('../models/company');

var isAuthenticated = function(req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/login');
};

module.exports = function(passport) {
    router.get('/', function(req, res) {
        res.render('index.ejs', {
            'title': '마인드엘리베이션'
        });
    });
    router.get('/index', function(req, res) {
        res.render('index.ejs', {
            'title': '마인드엘리베이션'
        });
    });
    router.get('/login', function(req, res) {
        res.render('login.ejs', {
            'title': '로그인',
            'message': req.flash('message_login')
        });
    });
    router.post('/companySearch', function(req, res) {
        Company.find({
                companyName: {
                    $regex: req.body.companyName
                }
            }, {
                _id: false,
                companyName: true,
                companyRep: true
            },
            function(err, company) {
                res.send({
                    'company': company
                });
            });
    });
    router.get('/notfound', function(req, res) {
        res.render('notfound.ejs', {
            'title': '404 Not found'
        });
    });
    router.post('/personalSignup', passport.authenticate('personalSignup', {
        successRedirect: '/login',
        failureRedirect: '/login', //가입 실패시 redirect할 url주소
        failureFlash: true
    }));
    router.post('/companySignup', passport.authenticate('companySignup', {
        successRedirect: '/login',
        failureRedirect: '/login', //가입 실패시 redirect할 url주소
        failureFlash: true
    }));
    router.post('/companyAdd', function(req, res) {
        Company.findOne({
            'companyName': req.body.companyName
        }, function(err, company) {
            if (err) {
                req.flash('message_login', 'DB 검색에 오류가 발생했습니다.');
                res.redirect('/login');
                return;
            }
            if (company) {
                console.log('[' + req.body.companyName + '] ' + 'Company already exists.');
                req.flash('message_login', '회사가 이미 존재합니다.');
                res.redirect('/login');
                return;
            }

            let newCompany = new Company();

            newCompany.companyName = req.body.companyName;
            newCompany.companyRep = req.body.companyRep;
            newCompany.businessReg = req.body.businessReg;
            newCompany.companyPost = req.body.companyPost;
            newCompany.companyAddress = req.body.companyAddress;
            newCompany.companyAddressSpec = req.body.companyAddressSpec;

            newCompany.save(function(err) {
                if (err) {
                    console.log('[' + req.body.companyName + '] ' + 'Error in Saving company: ' + err);
                    throw err;
                    return;
                }
                console.log('[' + req.body.companyName + '] ' + 'Company add succesful');
                return newCompany;
            });
        });
    });
    router.post('/login', passport.authenticate('login', {
        successRedirect: '/main',
        failureRedirect: '/login', //로그인 실패시 redirect할 url주소
        failureFlash: true
    }));
    router.get('/auth/google', passport.authenticate('google', {
        authType: 'rerequest',
        scope: ['profile', 'email']
    }));
    router.get('/auth/google/callback', passport.authenticate('google', {
        failureRedirect: '/login'
    }), function(req, res) {
        // console.log(req.query);
        res.redirect('/main');
    });
    router.get('/auth/facebook', passport.authenticate('facebook', {
        authType: 'rerequest',
        scope: ['public_profile', 'email']
    }));
    router.get('/auth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/login'
    }), function(req, res) {
        // console.log(req.query);
        res.redirect('/main');
    });
    router.get('/signout', function(req, res) {
        isAuthenticated(req, res, function() {
            console.log('[' + req.user.username + '] ' + 'Logout');
            req.logout();
            res.redirect('/login');
        });
    });

    return router;
};
