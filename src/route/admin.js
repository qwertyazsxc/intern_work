// router 분할 시 redirect 경로는 절대 경로로 적어주어야 정상 작동함.

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Company = require('../models/company');
var bCrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');

var isAdmin = function(req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated() && req.user.usertype == 'Admin')
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/admin/login');
};

var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
};

// Generates hash using bCrypt
var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

module.exports = function(passport) {
    router.get('/', function(req, res) {
        res.redirect('/admin/user');
    });
    router.get('/login', function(req, res) {
        // /admin/login 접속 시 admin 계정이 하나도 없다면 새로 생성
        User.findOne({
            'usertype': 'Admin'
        }, function(err, user) {
            if (!user) {
                let newUser = new User();
                username = password = 'admin';

                newUser.usertype = 'Admin';
                newUser.username = username;
                newUser.password = createHash(password);

                // save the user
                newUser.save(function(err) {
                    if (err) {
                        console.log('[' + newUser.username + '] ' + 'Error in Saving user: ' + err);
                        throw err;
                    }
                    console.log('[' + newUser.username + '] ' + 'User Registration succesful');
                    req.flash('message_admin', 'Admin 계정이 생성되었습니다.');
                    return;
                });
            }
        });

        res.render('admin/login.ejs', {
            'title': 'admin',
            'message': req.flash('message_admin')
        });
    });
    router.post('/login', passport.authenticate('adminLogin', {
        successRedirect: '/admin/user',
        failureRedirect: '/admin/login', //로그인 실패시 redirect할 url주소
        failureFlash: true
    }));
    router.get('/user', function(req, res) {
        isAdmin(req, res, function() {
            res.render('admin/user.ejs', {
                'title': 'admin',
                'user': req.user,
                'message': req.flash('message_admin')
            });
        });
    });
    router.post('/user', function(req, res) {
        isAdmin(req, res, function() {
            if (req.body.type == 'default') {
                User.find({
                        usertype: {
                            $ne: 'Admin'
                        }
                    }, {
                        _id: false,
                        usertype: true,
                        username: true,
                        fullname: true,
                        phone: true,
                        email: true,
                        gender: true,
                        chargeAmt: true
                    },
                    function(err, user) {
                        res.send({
                            'user': user
                        });
                    });
            } else {
                let query = {};
                let word = req.body.query;

                if (req.body.type == 'usertype') {
                    if (/(개인\(|^)구글/.test(req.body.query)) {
                        word = 'Google';
                    } else if (/(개인\(|^)페(?=$|이(?=$|스(?=$|북)))/.test(req.body.query)) {
                        word = 'Facebook';
                    } else if (/단(?=$|체)/.test(req.body.query)) {
                        word = 'Company';
                    } else if (/담(?=$|당(?=$|자))/.test(req.body.query)) {
                        word = 'Company-manager';
                    } else if (/구성(?=$|원)/.test(req.body.query)) {
                        word = /^Company(?!.)/;
                    } else if (/^구$/.test(req.body.query)) {
                        word = /^Company(?!.)|Google/;
                    } else if (/^개인/.test(req.body.query)) {
                        word = /Personal|Facebook|Google/;
                    }
                } else if (req.body.type == 'gender') {
                    if (/^남(?=$|자$)/.test(req.body.query)) {
                        word = /^male/;
                    } else if (/^여(?=$|자$)/.test(req.body.query)) {
                        word = 'female';
                    }
                }

                query[req.body.type] = {
                    $regex: word,
                    $options: 'i'
                };

                User.find(
                    query, {
                        _id: false,
                        usertype: true,
                        username: true,
                        fullname: true,
                        phone: true,
                        email: true,
                        gender: true,
                        chargeAmt: true
                    },
                    function(err, user) {
                        res.send({
                            'user': user
                        });
                    });
            }
        });
    });
    router.get('/company', function(req, res) {
        isAdmin(req, res, function() {
            res.render('admin/company.ejs', {
                'title': 'admin',
                'user': req.user,
                'message': req.flash('message_admin')
            });
        });
    });
    router.post('/company', function(req, res) {
        isAdmin(req, res, function() {
            if (req.body.type == 'default') {
                Company.find({}, {
                        _id: false,
                        companyName: true,
                        companyRep: true,
                        businessReg: true,
                        companyPost: true,
                        companyAddress: true,
                        companyAddressSpec: true,
                        groups: true
                    },
                    function(err, company) {
                        res.send({
                            'company': company
                        });
                    });
            } else {
                let query = {};
                let word = req.body.query;

                query[req.body.type] = {
                    $regex: word,
                    $options: 'i'
                };

                Company.find(
                    query, {
                        _id: false,
                        companyName: true,
                        companyRep: true,
                        businessReg: true,
                        companyPost: true,
                        companyAddress: true,
                        companyAddressSpec: true,
                        groups: true
                    },
                    function(err, company) {
                        res.send({
                            'company': company
                        });
                    });
            }
        });
    });
    router.get('/email', function(req, res) {
        isAdmin(req, res, function() {
            res.render('admin/email.ejs', {
                'title': '마인드엘리베이션',
                'message': req.flash('message_admin')
            });
        });
    });
    router.post("/email", function(req, res) {
        var emailArr = new Array();
        var subject = req.body.subject;
        var article = req.body.article;

        var transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com', // 회사
            port: 465,
            secure: true,
            auth: {
                user: 'user@mail.com', // 계정
                pass: 'password' // 비밀번호
            }
        });
        User.find({}, {
                _id: false,
                email: true
            },
            function(err, users) {
                for (var i = 0; i < users.length; i++) {
                    emailArr.push(users[i].email);

                    var mailOptions = {
                        from: 'user@mail.com', // 발송 메일 주소 (위에서 작성한 계정 아이디)
                        to: emailArr[i], // 수신 메일 주소
                        subject: subject, // 제목
                        text: article // 내용
                    };

                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                }
                console.log(emailArr);
            });
        res.redirect("/admin/email");
    });
    router.get('/password', function(req, res) {
        isAdmin(req, res, function() {
            res.render('admin/password.ejs', {
                'title': 'admin',
                'user': req.user,
                'message': req.flash('message_admin')
            });
        });
    });
    router.post('/password', function(req, res) {
        isAdmin(req, res, function() {
            User.findOne({
                'usertype': 'Admin',
                'username': req.body.username
            }, function(err, user) {
                if (err) {
                    req.flash('message_admin', 'DB 검색에 오류가 발생했습니다.');
                    res.redirect('/admin');
                    return;
                }
                if (!user) {
                    console.log('[' + req.user.username + '] ' + 'User Not Found');
                    req.flash('message_admin', '아이디가 존재하지 않습니다.');
                    res.redirect('/admin');
                    return;
                }
                if (!isValidPassword(user, req.body.current_pw)) {
                    console.log('[' + req.user.username + '] ' + 'current_pw Invalid');
                    req.flash('message_admin', '현재 비밀번호가 다릅니다.');
                    res.redirect('/admin');
                    return;
                }
                if (req.body.new_pw != req.body.new_pw_re) {
                    console.log('[' + req.user.username + '] ' + 'new_pw unmatched with new_pw_re');
                    req.flash('message_admin', '비밀번호 재입력이 맞지 않습니다.');
                    res.redirect('/admin');
                    return;
                }
                user.password = createHash(req.body.new_pw);

                // save the user
                user.save(function(err) {
                    if (err) {
                        console.log('[' + user.username + '] ' + 'Error in Saving user: ' + err);
                        throw err;
                    }
                    console.log('[' + user.username + '] ' + 'Password change succesful');
                    req.flash('message_admin', '비밀번호가 변경되었습니다.');
                    return user;
                });

                res.redirect('/admin');
            });
        });
    });

    return router;
};
