// router 분할 시 redirect 경로는 절대 경로로 적어주어야 정상 작동함.

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Company = require('../models/company');
var Group = require('../models/group');
var Checkup = require('../models/checkup');
var bCrypt = require('bcrypt-nodejs');

var isAuthenticated = function(req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/login');
};

var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
};

var fillZeroes = function(number) {
    var str = '0000' + number;
    str = str.slice(-4);

    return str;
}

// Generates hash using bCrypt
var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

var uuid = function() {
    function s4() {
        return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function findUser(user) {
    return new Promise(function(fulfill, reject) {
        User.find({
            'managerName': user.username
        }, function(err, member) {
            if (err) reject(err);
            else if (member) fulfill(member);
            else reject(new Error("유저가 존재 하지 않습니다."));
        });
    });
};

module.exports = function(passport) {
    router.get('/', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                username: req.user.username
            }, function(err, user) {
                res.render('main/welcome.ejs', {
                    'title': '마인드엘리베이션',
                    'user': user,
                    'message': req.flash('message_main')
                });
            });
        });
    });
    router.get('/info', function(req, res) {
        res.redirect('/main/authcheck');
    });
    router.get('/authcheck', function(req, res) {
        isAuthenticated(req, res, function() {
            if (req.user.usertype == 'Google' || req.user.usertype == 'Facebook') {
                res.render('main/info.ejs', {
                    'title': '마인드엘리베이션',
                    'user': req.user,
                    'message': req.flash('message_main')
                });
            } else {
                res.render('main/authcheck.ejs', {
                    'title': '마인드엘리베이션',
                    'user': req.user,
                    'message': req.flash('message_main')
                });
            }
        });
    });
    router.post('/authinfo', function(req, res) {
        // console.log('req: ', req);
        // console.log('res: ', res);
        isAuthenticated(req, res, function() {
            User.findOne({
                    'username': req.body.username
                },
                function(err, user) {
                    if (isValidPassword(user, req.body.password)) {
                        res.render('main/info.ejs', {
                            'title': '마인드엘리베이션',
                            'user': req.user,
                            'message': req.flash('message_main')
                        });
                    } else {
                        req.flash('비밀번호 재확인이 맞지 않습니다.');
                        res.redirect('/main');
                    }
                }
            );
        });
    });
    router.post('/infochange', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                    'username': req.body.username
                },
                function(err, user) {
                    if (user.usertype == 'Personal' || user.usertype == 'Google' || user.usertype == 'Facebook') {
                        user.fullname = req.body.fullname;
                        user.birthdate = req.body.birthdate;
                        user.phone = req.body.phone;
                        user.email = req.body.email;
                        user.gender = req.body.gender;

                        // save the user
                        user.save(function(err) {
                            if (err) {
                                console.log('[' + user.username + '] ' + 'Error in Saving user: ' + err);
                                throw err;
                            }
                            console.log('[' + user.username + '] ' + 'Userinfo change succesful');
                            return user;
                        });

                        res.render('main/welcome.ejs', {
                            'title': '마인드엘리베이션',
                            'user': user,
                            'message': req.flash('message_main')
                        });
                    } else if (user.usertype == 'Company-manager') {
                        user.fullname = req.body.fullname;
                        user.position = req.body.position;
                        user.department = req.body.department;
                        user.phone = req.body.phone;
                        user.email = req.body.email;

                        // save the user
                        user.save(function(err) {
                            if (err) {
                                console.log('[' + user.username + '] ' + 'Error in Saving user: ' + err);
                                throw err;
                            }
                            console.log('[' + user.username + '] ' + 'Userinfo change succesful');
                            return user;
                        });
                    }
                });
        });
    });
    router.get('/companyinfo', function(req, res) {
        isAuthenticated(req, res, function() {
            Company.findOne({
                    'companyName': req.user.companyName
                },
                function(err, company) {
                    res.render('main/companyinfo.ejs', {
                        'title': '마인드엘리베이션',
                        'user': req.user,
                        'company': company,
                        'message': req.flash('message_main')
                    });
                }
            );
        });
    });
    router.post('/companyinfochange', function(req, res) {
        isAuthenticated(req, res, function() {
            Company.findOne({
                    'companyName': req.body.companyName
                },
                function(err, company) {
                    company.companyRep = req.body.companyRep;
                    company.businessReg = req.body.businessReg;
                    company.companyPost = req.body.companyPost;
                    company.companyAddress = req.body.companyAddress;
                    company.companyAddressSpec = req.body.companyAddressSpec;

                    // save the company
                    company.save(function(err) {
                        if (err) {
                            console.log('[' + company.companyName + '] ' + 'Error in Saving company: ' + err);
                            throw err;
                        }
                        console.log('[' + company.companyName + '] ' + 'Companyinfo change succesful');
                        return company;
                    });

                    res.render('main/welcome.ejs', {
                        'title': '마인드엘리베이션',
                        'user': req.user,
                        'company': company,
                        'message': req.flash('message_main')
                    });
                }
            );
        });
    });
    router.get('/password', function(req, res) {
        isAuthenticated(req, res, function() {
            if (req.user.usertype == 'Google' || req.user.usertype == 'Facebook') {
                req.flash('message_main', '소셜 로그인한 경우 비밀번호를 변경할 수 없습니다.');
                res.redirect('/main');
                return;
            }
            res.render('main/password.ejs', {
                'title': '마인드엘리베이션',
                'user': req.user,
                'message': req.flash('message_main')
            });
        });
    });
    router.post('/password', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                    'username': req.body.username
                },
                function(err, user) {
                    if (err) {
                        req.flash('message_main', 'DB 검색에 오류가 발생했습니다.');
                        res.redirect('/main');
                        return;
                    }
                    if (!user) {
                        console.log('[' + req.user.username + '] ' + 'User Not Found');
                        req.flash('message_main', '아이디가 존재하지 않습니다.');
                        res.redirect('/main');
                        return;
                    }
                    if (!isValidPassword(user, req.body.current_pw)) {
                        console.log('[' + req.user.username + '] ' + 'current_pw Invalid');
                        req.flash('message_main', '현재 비밀번호가 다릅니다.');
                        res.redirect('/main');
                        return;
                    }
                    if (req.body.new_pw != req.body.new_pw_re) {
                        console.log('[' + req.user.username + '] ' + 'new_pw unmatched with new_pw_re');
                        req.flash('message_main', '비밀번호 재입력이 맞지 않습니다.');
                        res.redirect('/main');
                        return;
                    }
                    user.password = createHash(req.body.new_pw);

                    // save the user
                    user.save(function(err) {
                        if (err) {
                            console.log('[' + user.username + '] ' + 'Error in Saving user: ' + err);
                            throw err;
                        }
                        console.log('[' + user.username + '] ' + 'Password change succesful');
                        req.flash('message_main', '비밀번호가 변경되었습니다.');
                        return user;
                    });

                    res.redirect('/main');
                }
            );
        });
    });
    router.post('/charge', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                    'username': req.user.username
                },
                function(err, user) {
                    if (err) {
                        req.flash('message_main', 'DB 검색에 오류가 발생했습니다.');
                        res.redirect('/main');
                        return;
                    }
                    if (!user) {
                        console.log('[' + req.user.username + '] ' + 'User Not Found');
                        req.flash('message_main', '아이디가 존재하지 않습니다.');
                        res.redirect('/main');
                        return;
                    }

                    if (req.body.chargeAmt == '') {
                        console.log('[' + req.user.username + '] ' + 'Empty input sent');
                        req.flash('message_main', '충전 금액을 입력해 주십시오.')
                        res.redirect('/main/prepayment')
                        return;
                    } else if (parseInt(req.body.chargeAmt) < 10) {
                        console.log('[' + req.user.username + '] ' + 'Input less than 10');
                        req.flash('message_main', '충전은 10원 이상 가능합니다.')
                        res.redirect('/main/prepayment')
                        return;
                    }
                    user.chargeAmt += parseInt(req.body.chargeAmt);

                    let paymentWays;

                    switch (req.body.paymentWays) {
                        case 'creditCard':
                            paymentWays = '신용카드';
                            break;
                        case 'bankTransfer':
                            paymentWays = '실시간 계좌이체';
                            break;
                        case 'virtualAccount':
                            paymentWays = '가상계좌';
                            break;
                    }

                    let newPay = {
                        'time': new Date().toString(),
                        'detail': '선불충전(' + paymentWays + ')',
                        'deposit': req.body.chargeAmt,
                        'withdraw': 0,
                        'chargeAmt': user.chargeAmt
                    }

                    user.payment.push(newPay);

                    // save the user
                    user.save(function(err) {
                        if (err) {
                            console.log('[' + user.username + '] ' + 'Error in Saving user: ' + err);
                            req.flash('message_main', 'DB 저장에 오류가 발생했습니다.');
                            res.redirect('/main');
                            throw err;
                        }
                        console.log('[' + user.username + '] ' + 'Charge succesful');
                        return user;
                    });

                    res.redirect('/main');
                }
            );
        });
    });
    router.get('/prepayment', function(req, res) {
        isAuthenticated(req, res, function() {
            res.render('main/prepayment.ejs', {
                'title': '마인드엘리베이션',
                'user': req.user,
                'message': req.flash('message_main')
            });
        });
    });
    router.get('/payhistory', function(req, res) {
        isAuthenticated(req, res, function() {
            res.render('main/payhistory.ejs', {
                'title': '마인드엘리베이션',
                'user': req.user,
                'message': req.flash('message_main')
            });
        });
    });
    router.get('/memreg', function(req, res) {
        isAuthenticated(req, res, function() {
            res.render('main/memreg.ejs', {
                'title': '마인드엘리베이션',
                'user': req.user,
                'message': req.flash('message_main')
            });
        });
    });

    router.post('/memreg', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                    'username': req.user.username
                },
                function(err, user) {
                    let Arr = req.body.members;

                    Group.findOne({
                            'managerName': req.user.username
                        },
                        function(err, group) {
                            for (let i = 0; i < Arr.length; i++) {
                                newUser = new User();

                                newUser.usertype = 'Company';

                                if (group.members.length > 0) {
                                    let lastnumber = group.members[group.members.length - 1].username;
                                    lastnumber = parseInt(lastnumber.substr(8, 4));

                                    if (lastnumber >= group.members.length) {
                                        newUser.username = 'mind' + fillZeroes(group.tracknum) + fillZeroes(lastnumber + 1);

                                    } else {
                                        newUser.username = 'mind' + fillZeroes(group.tracknum) + fillZeroes(group.members.length);
                                    }

                                } else {
                                    newUser.username = 'mind' + fillZeroes(group.tracknum) + fillZeroes(group.members.length);
                                }
                                newUser.password = createHash(newUser.username);
                                newUser.birthdate = Arr[i].birthdate;
                                newUser.fullname = Arr[i].fullname;
                                newUser.phone = Arr[i].phone;
                                newUser.email = Arr[i].email;
                                newUser.gender = Arr[i].gender;
                                newUser.companyName = user.companyName;
                                newUser.managerName = user.username;

                                group.members.push({
                                    'username': newUser.username
                                });

                                newUser.save(function(err) {
                                    if (err) {
                                        console.log('[' + user.username + '] ' + 'Error in Saving new member: ' + err);
                                        throw err;
                                    }
                                    return;
                                });
                            }

                            group.save(function(err) {
                                if (err) {
                                    console.log('[' + user.username + '] ' + 'Error in Saving group: ' + err);
                                    throw err;
                                }
                                console.log('[' + user.username + '] ' + 'Members add succesful!');
                                return;
                            });
                        });
                });
            res.redirect('/main');
        });
    });

    router.get('/memlookup', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                'username': req.user.username,
            }, function(err, user) {
                findUser(user).then(function(member) {
                    //console.log(member);
                    res.render('main/memlookup.ejs', {
                        'title': '마인드엘리베이션',
                        'user': req.user,
                        'member': member,
                        'message': req.flash('message_main')
                    });
                }, function(err) {
                    if (err) {
                        req.flash('message_main', 'DB 조회에서 오류가 발생했습니다.');
                        res.redirect('/main');
                        return;
                    }
                });
            });
        });
    });
    router.post('/memdelete', function(req, res) {
        isAuthenticated(req, res, function() {
            Group.findOne({
                'managerName': req.user.username
            }, function(err, check) {
                for (i = 0; i < req.body.delmembers.length; i++) {
                    if (check.tracknum != parseInt(req.body.delmembers[i].substr(4, 4))) {
                        req.flash('message_main', 'DB 삭제에서 오류가 발생했습니다.');
                        res.redirect('/main');
                        console.log("Abnormal approach");
                        return;
                    } else {
                        User.remove({
                            'username': req.body.delmembers
                        }, function(err, user) {
                            if (err) {
                                req.flash('message_main', 'DB 삭제에서 오류가 발생했습니다.');
                                res.redirect('/main');
                                return;
                            }
                            console.log("Delete succesful");
                        });

                        //그룹 members에서 제거.
                        for (let j = 0; j < req.body.delmembers.length; j++) {
                            Group.update({
                                'managerName': req.user.username
                            }, {
                                $pull: {
                                    'members': {
                                        'username': req.body.delmembers[j]
                                    }
                                }
                            }, function(err, user) {
                                if (err) {
                                    req.flash('message_main', 'DB 삭제에서 오류가 발생했습니다.');
                                    res.redirect('/main');
                                    return;
                                }
                                console.log("Delete succesful in DB");
                            });
                        }
                    }
                }
            });
        });
    });
    router.get('/checkup', function(req, res) {
        isAuthenticated(req, res, function() {
            res.render('main/checkup.ejs', {
                'title': '마인드엘리베이션',
                'user': req.user,
                'message': req.flash('message_main')
            });
        });
    });
    router.post('/members', function(req, res) {
        isAuthenticated(req, res, function() {
            let query = {};
            let word = req.body.query;

            query['managerName'] = req.user.username;

            if (req.body.type != 'default') {
                query[req.body.type] = {
                    $regex: word,
                    $options: 'i'
                };
            }

            User.find(query).then((user) => {
                res.send({
                    'members': user
                });
            }).catch((err) => {
                req.flash('message_main', 'DB 검색에 오류가 발생했습니다.');
                res.redirect('/main/checkup');
            });
        });
    });
    router.post('/personalCheckup', function(req, res) {
        isAuthenticated(req, res, function() {
            User.findOne({
                    'username': req.user.username
                },
                function(err, user) {
                    if (err) {
                        req.flash('message_main', 'DB 검색에 오류가 발생했습니다.');
                        res.redirect('/main');
                        return;
                    }
                    if (!user) {
                        console.log('[' + req.user.username + '] ' + 'User Not Found');
                        req.flash('message_main', '아이디가 존재하지 않습니다.');
                        res.redirect('/main');
                        return;
                    }

                    if (user.chargeAmt < 10000) {
                        console.log('[' + user.username + '] ' + 'Lack of chargeAmt: ' + user.chargeAmt);
                        req.flash('message_main', '마인드캐시 잔액이 부족합니다.');
                        res.redirect('/main/checkup');
                        return;
                    }
                    user.chargeAmt -= 10000;

                    let newPay = {
                        'time': new Date().toString(),
                        'detail': '검사신청',
                        'deposit': 0,
                        'withdraw': 10000,
                        'chargeAmt': user.chargeAmt
                    }
                    user.payment.push(newPay);

                    // save the user
                    user.save(function(err) {
                        if (err) {
                            console.log('[' + user.username + '] ' + 'Error in Saving user: ' + err);
                            req.flash('message_main', 'DB 저장에 오류가 발생했습니다.');
                            res.redirect('/main');
                            throw err;
                        }
                        console.log('[' + user.username + '] ' + 'Checkup request succesful');
                        return user;
                    });

                    let newCheckup = new Checkup();

                    // status와 temper 및 resultPath는 검사신청 시에는 비어있는 것이 맞음
                    // test를 위해 임의의 값을 주고 진행
                    newCheckup.username = user.username;
                    newCheckup.fullname = user.fullname;
                    newCheckup.status = 'complete';
                    newCheckup.time = new Date().toString();
                    let temperArrForTest = new Array('happy', 'sad', 'annoy', 'anxious');
                    newCheckup.temper = temperArrForTest[Math.floor(Math.random() * 4)];
                    newCheckup.resultPath = 'results/sample.pdf';

                    newCheckup.save(function(err) {
                        if (err) {
                            console.log('[' + user.username + '] ' + 'Error in Saving checkup: ' + err);
                            req.flash('message_main', 'DB 저장에 오류가 발생했습니다.');
                            res.redirect('/main');
                            throw err;
                        }
                        return user;
                    });

                    res.redirect('/main');
                }
            );
        });
    });
    router.get('/result', function(req, res) {
        isAuthenticated(req, res, function() {
            Checkup.find({
                'username': req.user.username
            }, function(err, checkup) {
                res.render('main/result.ejs', {
                    'title': '마인드엘리베이션',
                    'user': req.user,
                    'checkup': checkup,
                    'message': req.flash('message_main')
                });
            });
        });
    });
    router.post('/popup', function(req, res) {
        res.pdf(path.resolve(__dirname, '../' + req.body.resultPath));
    });

    return router;
};
