var mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
    // 단체명
    companyName: String,
    // 대표자명
    companyRep: String,
    // 사업자 등록 번호
    businessReg: String,
    // 우편번호
    companyPost: String,
    // 주소
    companyAddress: String,
    // 상세주소
    companyAddressSpec: String,
    //
    groups: [{
        department: String
    }]
});

module.exports = mongoose.model('Company', companySchema);
