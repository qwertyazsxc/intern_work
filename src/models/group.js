var mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
    // 연번
    tracknum: Number,
    // 소속 단체
    companyName: String,
    // 부서명
    department: String,
    // 담당자 ID
    managerName: String,
    // 구성원
    members: [{
        username: String,
    }]
});

module.exports = mongoose.model('Group', groupSchema);
