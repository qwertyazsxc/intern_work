var mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    /////////////////////////////// 공통 ///////////////////////////////
    usertype: String,
    username: String,
    password: String,
    fullname: String,
    birthdate: Date,
    phone: String,
    email: String,
    gender: String,
    chargeAmt: Number,
    payment: [{
        time: Date,
        detail: String,
        deposit: Number,
        withdraw: Number,
        chargeAmt: Number
    }],

    /////////////////////////// 단체 담당자 ////////////////////////////
    companyName: String,
    position: String,
    department: String,

    /////////////////////////// 단체 구성원 ////////////////////////////
    managerName: String
});

module.exports = mongoose.model('User', userSchema);
