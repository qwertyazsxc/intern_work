var mongoose = require('mongoose');

const settingsSchema = new mongoose.Schema({
    // group tracknum 중 마지막
    groupTrackNum: Number
});

module.exports = mongoose.model('Settings', settingsSchema);
