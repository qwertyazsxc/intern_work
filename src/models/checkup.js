var mongoose = require('mongoose');

const checkupSchema = new mongoose.Schema({
    // 회원 ID
    username: String,
    // 회원 이름
    fullname: String,
    // 검사 상태(검사 진행중 / 검사 완료)
    status: String,
    // 신청 시간
    time: Date,
    // 심리 상태(행복, 우울, ...)
    temper: String,
    // 결과해석지 파일 경로
    resultPath: String
});

module.exports = mongoose.model('Checkup', checkupSchema);
