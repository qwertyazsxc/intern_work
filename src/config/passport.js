var LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../models/user');
var Group = require('../models/group');
var Company = require('../models/company');
var Settings = require('../models/settings');
var bCrypt = require('bcrypt-nodejs');
const GOOGLE_CLIENT_ID = '1054563130149-i9k6d231shtvjjcjps7456jsq33gb4c6.apps.googleusercontent.com';
const GOOGLE_CLIENT_SECRET = 'tBsK02ARCJsCCTOnKBQ1FthM';
const FACEBOOK_CLIENT_ID = '328018294379385';
const FACEBOOK_CLIENT_SECRET = '90350b6bf742f48b82015e4819e14c3d';

module.exports = function(passport) {
    // serialize and desrialize user
    passport.serializeUser(function(user, done) {
        // console.log('serializing user: ' + user.username);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            // console.log('deserializing user: ' + user.username);
            done(err, user);
        });
    });

    // admin 계정 생성
    passport.use('adminSignup', new LocalStrategy({
        passReqToCallback: true
    }, function(req, username, password, done) {
        findOrCreateUser = function() {
            // find a user in Mongo with provided username
            User.findOne({
                'username': username
            }, function(err, user) {
                // In case of any error, return using the done method
                if (err) {
                    console.log('[' + username + '] ' + 'Error in SignUp: ' + err);
                    return done(err);
                }
                // already exists
                if (user) {
                    console.log('[' + username + '] ' + 'User already exists');
                    return done(null, false, req.flash('message_admin', '아이디가 이미 존재합니다.'));
                } else {
                    // if there is no user with that email
                    // create the user
                    var newUser = new User();

                    // set the user's local credentials
                    newUser.usertype = 'Admin';
                    newUser.username = username;
                    newUser.password = createHash(password);

                    // save the user
                    newUser.save(function(err) {
                        if (err) {
                            console.log('[' + username + '] ' + 'Error in Saving user: ' + err);
                            throw err;
                        }
                        console.log('[' + username + '] ' + 'User Registration succesful');
                        req.flash('message_admin', 'Admin 계정이 생성되었습니다.');
                        return done(null, newUser);
                    });
                }
            });
        };
        // Delay the execution of findOrCreateUser and execute the method
        // in the next tick of the event loop
        process.nextTick(findOrCreateUser);
    }));

    passport.use('adminLogin', new LocalStrategy({
            passReqToCallback: true
        },
        function(req, username, password, done) {
            // check in mongo if a user with username exists or not
            User.findOne({
                    'username': username,
                    'usertype': 'Admin'
                },
                function(err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Username does not exist, log the error and redirect back
                    if (!user) {
                        console.log('[' + username + '] ' + 'User Not Found');
                        return done(null, false, req.flash('message_admin', '아이디가 존재하지 않습니다.'));
                    }
                    // User exists but wrong password, log the error
                    if (!isValidPassword(user, password)) {
                        console.log('[' + username + '] ' + 'Invalid Password');
                        return done(null, false, req.flash('message_admin', '비밀번호가 틀렸습니다.')); // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success
                    console.log('[' + username + '] ' + "Admin Login");
                    return done(null, user);
                }
            );
        }));

    // local login
    passport.use('personalSignup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            findOrCreateUser = function() {
                // find a user in Mongo with provided username
                User.findOne({
                    'username': username
                }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.log('[' + username + '] ' + 'Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('[' + username + '] ' + 'User already exists');
                        return done(null, false, req.flash('message_login', '아이디가 이미 존재합니다.'));
                    } else {
                        // if there is no user with that email
                        // create the user
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.usertype = 'Personal';
                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.birthdate = req.body.birthdate;
                        newUser.fullname = req.body.fullname;
                        newUser.phone = req.body.phone;
                        newUser.email = req.body.email;
                        newUser.gender = req.body.gender;
                        newUser.chargeAmt = 0;

                        // save the user
                        newUser.save(function(err) {
                            if (err) {
                                console.log('[' + username + '] ' + 'Error in Saving user: ' + err);
                                throw err;
                            }
                            console.log('[' + username + '] ' + 'User Registration succesful');
                            req.flash('message_login', '가입에 성공했습니다.');
                            return done(null, newUser);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        }));

    passport.use('companySignup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            findOrCreateUser = function() {
                // find a user in Mongo with provided username
                User.findOne({
                    'username': username
                }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.log('[' + username + '] ' + 'Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('[' + username + '] ' + 'User already exists');
                        return done(null, false, req.flash('message_login', '아이디가 이미 존재합니다.'));
                    } else {
                        // if there is no user with that username
                        // create the user
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.usertype = 'Company-manager';
                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.fullname = req.body.fullname;
                        newUser.companyName = req.body.companyName;
                        newUser.position = req.body.position;
                        newUser.department = req.body.department;
                        newUser.phone = req.body.phone;
                        newUser.email = req.body.email;

                        Settings.findOne({}, function(err, settings) {
                            if (err) {
                                req.flash('message_login', 'DB 검색에 오류가 발생했습니다.');
                                res.redirect('/login');
                                return;
                            }
                            if (!settings) {
                                var newSettings = new Settings();
                                newSettings.groupTrackNum = 1;
                                newSettings.save(function(err) {
                                    if (err) {
                                        throw err;
                                        return;
                                    }
                                    return newSettings;
                                });
                            } else settings.update({
                                $set: {
                                    'groupTrackNum': settings.groupTrackNum + 1
                                }
                            }, function(err, result) {
                                if (err) console.log('Error in updating groupTrackNum: ' + err);
                                return;
                            });

                            var newGroup = new Group();

                            newGroup.tracknum = (settings) ? settings.groupTrackNum + 1 : 1;
                            newGroup.department = newUser.department;
                            newGroup.managerName = newUser.username;

                            newGroup.save(function(err) {
                                if (err) {
                                    console.log('[' + username + '] ' + 'Error in Saving group: ' + err);
                                    throw err;
                                }
                                return;
                            });

                            Company.findOne({
                                'companyName': newUser.companyName
                            }, function(err, company) {
                                company.groups.push({
                                    'department': newUser.department
                                });

                                company.save(function(err) {
                                    if (err) {
                                        console.log('[' + username + '] ' + 'Error in Saving company: ' + err);
                                        throw err;
                                    }
                                    return;
                                });
                            });

                            // save the user
                            newUser.save(function(err) {
                                if (err) {
                                    console.log('[' + username + '] ' + 'Error in Saving user: ' + err);
                                    throw err;
                                }
                                console.log('[' + username + '] ' + 'User Registration succesful');
                                req.flash('message_login', '가입에 성공했습니다.');
                                return done(null, newUser);
                            });
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        }));

    // Generates hash using bCrypt
    var createHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };

    passport.use('login', new LocalStrategy({
            passReqToCallback: true
        },
        function(req, username, password, done) {
            var usertype;

            if (req.body.usertype == 'Personal') usertype = 'Personal';
            else usertype = /Company.*/;

            // check in mongo if a user with username exists or not
            User.findOne({
                    'username': username,
                    'usertype': usertype
                },
                function(err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Username does not exist, log the error and redirect back
                    if (!user) {
                        console.log(usertype);
                        console.log('[' + username + '] ' + 'User Not Found');
                        return done(null, false, req.flash('message_login', '아이디가 존재하지 않습니다.'));
                    }
                    // User exists but wrong password, log the error
                    if (!isValidPassword(user, password)) {
                        console.log('[' + username + '] ' + 'Invalid Password');
                        return done(null, false, req.flash('message_login', '비밀번호가 틀렸습니다.')); // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success
                    console.log('[' + username + '] ' + "Login");
                    return done(null, user);
                }
            );
        }));

    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    };

    // social login
    passport.use('google', new GoogleStrategy({
            clientID: GOOGLE_CLIENT_ID,
            clientSecret: GOOGLE_CLIENT_SECRET,
            // 개발용
            callbackURL: "http://localhost:8080/auth/google/callback",
            // 서비스용
            //callbackURL: "http://snorlaxious.iptime.org:8080/auth/google/callback",
            passReqToCallback: true
        },
        function(req, accessToken, refreshToken, profile, done) {
            // console.log(profile);
            var username = 'google' + profile.id;

            findOrCreateUser = function() {
                // find a user in Mongo with provided username
                User.findOne({
                    'username': username
                }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.log('[' + username + '] ' + 'Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('[' + username + '] ' + "Login");
                        return done(null, user);
                    } else {
                        // if there is no user with that username
                        // create the user
                        var newUser = new User();

                        newUser.usertype = 'Google';
                        newUser.username = username;
                        newUser.fullname = profile.displayName;
                        newUser.email = profile.emails[0].value;
                        newUser.gender = profile.gender;
                        newUser.chargeAmt = 0;

                        // save the user
                        newUser.save(function(err) {
                            if (err) {
                                console.log('[' + username + '] ' + 'Error in Saving user: ' + err);
                                throw err;
                            }
                            console.log('[' + username + '] ' + 'User Registration succesful');
                            console.log('[' + username + '] ' + "Login");
                            return done(null, newUser);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        }
    ));

    passport.use('facebook', new FacebookStrategy({
            clientID: FACEBOOK_CLIENT_ID,
            clientSecret: FACEBOOK_CLIENT_SECRET,
            // 개발용
            callbackURL: "http://localhost:8080/auth/facebook/callback",
            // 서비스용
            //callbackURL: "http://snorlaxious.iptime.org:8080/auth/facebook/callback",
            passReqToCallback: true
        },
        function(req, accessToken, refreshToken, profile, done) {
            // console.log(profile);
            var username = 'facebook' + profile.id;

            findOrCreateUser = function() {
                // find a user in Mongo with provided username
                User.findOne({
                    'username': username
                }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.log('[' + username + '] ' + 'Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('[' + username + '] ' + "Login");
                        return done(null, user);
                    } else {
                        // if there is no user with that username
                        // create the user
                        var newUser = new User();

                        newUser.usertype = 'Facebook';
                        newUser.username = username;
                        newUser.fullname = profile.displayName;
                        newUser.gender = profile.gender;
                        newUser.chargeAmt = 0;

                        // save the user
                        newUser.save(function(err) {
                            if (err) {
                                console.log('[' + username + '] ' + 'Error in Saving user: ' + err);
                                throw err;
                            }
                            console.log('[' + username + '] ' + 'User Registration succesful');
                            console.log('[' + username + '] ' + "Login");
                            return done(null, newUser);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        }
    ));
};
